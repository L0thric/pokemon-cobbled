package com.cablemc.pokemoncobbled.common.item

import net.minecraft.item.Item

/**
 * Base for custom items in Cobbled.
 *
 * Containing common shared code.
 */
open class CobbledItem(settings : Settings) : Item(settings)