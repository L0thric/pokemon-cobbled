package com.cablemc.pokemoncobbled.common.battles

object BattleRules {
    const val OBTAINABLE = "Obtainable"
    const val TEAM_PREVIEW = "Team Preview"
    const val ENDLESS_BATTLE_CLAUSE = "Endless Battle Clause"
    const val CANCEL_MOD = "Cancel Mod"
    const val SLEEP_CLAUSE = "Sleep Clause"
    const val HP_PERCENTAGE_MOD = "HP Percentage Mod"
}