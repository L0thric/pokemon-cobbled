package com.cablemc.pokemoncobbled.common.api.gui

object ColourLibrary {
    const val WHITE = 0xFFFFFF
    const val BUTTON_HOVER_COLOUR = 0xB5C42F
    const val BUTTON_NORMAL_COLOUR = 0xFFFFFF

    const val SIDE_1_BATTLE_COLOUR = 0x0094FF
    const val SIDE_1_ALLY_BATTLE_COLOUR = 0x00918F
    const val SIDE_2_BATTLE_COLOUR = 0xD83838
    const val SIDE_2_ALLY_BATTLE_COLOUR = 0x6A48AC
}