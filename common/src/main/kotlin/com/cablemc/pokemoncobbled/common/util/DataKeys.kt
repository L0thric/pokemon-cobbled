package com.cablemc.pokemoncobbled.common.util

object DataKeys {
    const val POKEMON = "Pokemon"
    const val POKEMON_UUID = "UUID"
    const val POKEMON_SPECIES_DEX = "SpeciesDex"
    const val POKEMON_FORM_ID = "FormId"
    const val POKEMON_LEVEL = "Level"
    const val POKEMON_GENDER = "Gender"
    const val POKEMON_EXPERIENCE = "Experience"
    const val POKEMON_FRIENDSHIP = "Friendship"

    const val POKEMON_STATS = "Stats"
    const val POKEMON_IVS = "IVs"
    const val POKEMON_EVS = "EVs"
    const val POKEMON_HEALTH = "Health"
    const val POKEMON_SCALE_MODIFIER = "ScaleModifier"
    const val POKEMON_MOVESET = "MoveSet"
    const val POKEMON_MOVESET_MOVENAME = "MoveName"
    const val POKEMON_MOVESET_MOVEPP = "MovePP"
    const val POKEMON_MOVESET_RAISED_PP_STAGES = "RaisedPPStages"
    const val POKEMON_ABILITY = "Ability"
    const val POKEMON_ABILITY_NAME = "AbilityName"
    const val POKEMON_SHINY = "Shiny"
    const val POKEMON_STATUS = "Status"
    const val POKEMON_STATUS_NAME = "StatusName"
    const val POKEMON_STATUS_TIMER = "StatusTimer"
    const val POKEMON_CAUGHT_BALL = "CaughtBall"
    const val POKEMON_FAINTED_TIMER = "FaintedTimer"
    const val POKEMON_HEALING_TIMER = "HealingTimer"
    const val POKEMON_DATA = "PokemonData"

    const val POKEMON_STATE = "State"
    const val POKEMON_STATE_TYPE = "StateType"
    const val POKEMON_STATE_SHOULDER = "StateShoulder"
    const val POKEMON_STATE_ID = "StateId"
    const val POKEMON_STATE_PLAYER_UUID = "PlayerUUID"
    const val POKEMON_STATE_POKEMON_UUID = "PokemonUUID"

    // Evolution stuff
    const val POKEMON_EVOLUTIONS = "Evolutions"
    const val POKEMON_PENDING_EVOLUTIONS = "Pending"

    const val BENCHED_MOVES = "BenchedMoves"

    const val STORE_SLOT = "Slot"
    const val STORE_SLOT_COUNT = "SlotCount"
    const val STORE_BOX = "Box"
    const val STORE_BOX_COUNT = "BoxCount"
    const val STORE_BOX_COUNT_LOCKED = "BoxCountLocked"
    const val STORE_BACKUP = "BackupStore"

    /* Form stuff */
    const val ALOLAN = "Alolan"
    const val CRYSTAL = "Crystal"
    /* ---------- */

    /* Battle keys */
    const val REQUEST_TYPE = "RequestType"
    const val REQUEST_BATTLE_ID = "RequestBattleId"
    const val REQUEST_BATTLE_START = "StartBattle"
    const val REQUEST_BATTLE_SEND_MESSAGE = "SendMessage"
    const val REQUEST_MESSAGES = "RequestMessages"
    /* ----------- */

    const val POKEMON_PROPERTIES = "Properties"
    const val POKEMON_PROPERTIES_CUSTOM = "CustomProperties"
    const val POKEMON_PROPERTIES_ORIGINAL_TEXT = "OriginalText"
    const val POKEMON_SPECIES_TEXT = "SpeciesText"

    /* Healer  Block */
    const val HEALER_MACHINE_USER = "MachineUser"
    const val HEALER_MACHINE_POKEBALLS = "MachinePokeBalls"
    const val HEALER_MACHINE_TIME_LEFT = "MachineTimeLeft"
    const val HEALER_MACHINE_CHARGE = "MachineCharge"
    /* ----------- */

}