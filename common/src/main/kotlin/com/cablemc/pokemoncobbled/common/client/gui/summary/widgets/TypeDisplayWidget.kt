package com.cablemc.pokemoncobbled.common.client.gui.summary.widgets

import com.cablemc.pokemoncobbled.common.util.cobbledResource

class TypeDisplayWidget {

    companion object {

        private val TYPES_RESOURCE = cobbledResource("ui/types.png")

    }

}