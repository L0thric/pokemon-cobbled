package com.cablemc.pokemoncobbled.common.pokemon.status

import com.cablemc.pokemoncobbled.common.api.pokemon.status.Status
import net.minecraft.util.Identifier

/**
 * Represents a status that only remains during a battle.
 *
 * @author Deltric
 */
class VolatileStatus(name: Identifier) : Status(name) {

}