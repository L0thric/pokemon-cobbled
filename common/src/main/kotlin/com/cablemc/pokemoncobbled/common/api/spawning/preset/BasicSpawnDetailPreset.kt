package com.cablemc.pokemoncobbled.common.api.spawning.preset

class BasicSpawnDetailPreset : SpawnDetailPreset() {
    companion object {
        const val NAME = "basic"
    }
}