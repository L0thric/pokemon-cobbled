package com.cablemc.pokemoncobbled.common.api.item

import net.minecraft.util.Identifier

class Berry(
    val name: Identifier,
    val spicy: Int,
    val dry: Int,
    val sweet: Int,
    val bitter: Int,
    val sour: Int
) {

}