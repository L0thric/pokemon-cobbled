package com.cablemc.pokemoncobbled.common.api.battles.model.actor

/**
 * The type of actor it is. This is used for defining PvP, PvW, etc.
 *
 * @author Hiroku
 * @since July 1st, 2022
 */
enum class ActorType {
    WILD,
    PLAYER,
    NPC
}