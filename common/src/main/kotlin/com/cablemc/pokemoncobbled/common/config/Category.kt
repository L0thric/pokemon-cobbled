package com.cablemc.pokemoncobbled.common.config

enum class Category {
    Starter,
    Pokemon,
    Spawning,
    Battles,
    PassiveStatus,
    Healing,
    Storage
}